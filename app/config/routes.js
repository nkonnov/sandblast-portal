var React = require('react');
var Main = require('../components/Main');
var Home = require('../components/Home');
var FileDetail = require('../components/FileDetail');
var Router = require('react-router');
var Route = Router.Route;
var IndexRoute = Router.IndexRoute;

module.exports = (
        <Route path="/" component={Main}>
                 <Route path="file/:filename" component={FileDetail} />
                 <IndexRoute component={Home} />
        </Route>
);