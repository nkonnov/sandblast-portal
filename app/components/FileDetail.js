var React = require('react');
var FileDetail = React.createClass({
    render: function() {
        //console.log(this, this.props);
        return (
            <h2 className="text-center">
                FileDetail <b> {this.props.params.filename} </b>
            </h2>
        )
    }
});

module.exports = FileDetail;